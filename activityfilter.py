from helpers import *

import networkx as nx
import itertools
import re
import unittest
import math
import matplotlib
import matplotlib.pyplot as plt
import time

class ActivityFilter:
    def __init__(self, input_file, meters_per_month=None, depth_to_forward_ratio=None, max_elevation=None):
        if(meters_per_month is None):
            self._meters_per_month = 100
        else:
            self._meters_per_month = meters_per_month

        if(depth_to_forward_ratio is None):
            self._depth_to_forward_ratio = 1/14
        else:
            self._depth_to_forward_ratio = depth_to_forward_ratio

        self._depth_per_month = self._meters_per_month * self._depth_to_forward_ratio
        self._max_elevation = max_elevation
        self.esa = None

        self._create_graph(input_file)

    # Creates a directed graph where:
    # Each node is an activity
    # Each node links to its successors
    def _create_graph(self, input_file):
        with open(input_file,'r') as infile:
            lines = infile.readlines() 

            # get a list of the lines that begin with 'set PREQ'
            preq_lines = list(filter(lambda ln: 'set PREQ' in ln, lines))

            # use a regular expression to parse out each activity and its prerequesite
            preq_dict = {}
            for line in preq_lines:

                # Parses out an activity and its prerequesites from each line
                items = re.search('\[(.+)\]\s*:=\s*(.*);', line)

                cur_activity = items.group(1).strip()

                # Make a list of the prerequesites for an activity
                activity_preqs = list(
                    filter(lambda x: x.strip(), items.group(2).split())
                )

                preq_dict[cur_activity] = activity_preqs # (activity, its prerequesites)

            # save list of activities for external use
            self._activities = [ activity for activity in preq_dict ]

            # Get dict pairing panels to activities
            self._panels_acts = {}
            pta_lines = list(filter(lambda ln: 'set PANEL_TO_ACTIVITY' in ln, lines)) 
            for line in pta_lines:
                # Parse out Panel and its associated activities
                items = re.search('\[(.+), .+\]\s*:=\s*(.*);', line)

                panel_no = items.group(1).strip()

                panel_acts = list(
                    filter(lambda x: x.strip(), items.group(2).split())
                )

                # Add these activities to the dictionary under this panel
                if(panel_no not in self._panels_acts):
                    self._panels_acts[panel_no] = []
                self._panels_acts[panel_no].extend(panel_acts)

            # Initialize graph
            self._graph = nx.DiGraph()
            self._graph_roots = []

            # Loop through activities in preq structure
            for activity in preq_dict:

                debugp(f'activity: {activity}, preqs: {preq_dict[activity]}')

                # loop through the list of preqs for this activity
                for preq in preq_dict[activity]:

                    # Link this activity to each of its preqs
                    # (adding the nodes for the preqs if they do not already exist)
                    self._graph.add_edge(preq, activity)

                # If this activity is a root, add it to the roots
                if(preq_dict[activity] == []):
                    # This activity has no prereq, it is a root
                    self._graph_roots.append(activity)

    def find_earliest_times(self):
        if (self.esa is not None):
            return self.esa
        else:
            earliest_from_each_root = []

            # For each of the declines (these are the roots of the subgraphs):
            for root in self._graph_roots:

                # Define the max/starting elevation, this will be used later for
                # calculations involing time to reach a certain depth
                self._max_elevation = decline_elevation(root)

                # Run the recursive traverse_graph function, which will traverse
                # the graph of all activities (starting from this defined root)
                # and calculate the earliest time each one could be started.
                #
                # This information is returned as a dict, mapping 
                # {activity: earliest time} for each activity in the structure
                earliest_times = self._traverse_graph(root, decline_elevation(root))

                earliest_from_each_root.append(earliest_times)



            # Combine the dicts holding the earliest from each root into one dict
            # Which has the earliest time possible for all activities from any root
            combined = earliest_from_each_root[0]
            for d in range(1,len(earliest_from_each_root)):
                combined = merge_dicts(combined, earliest_from_each_root[d])

            self.esa = combined
            return combined

    def max_tp(self):
        esa_dict = self.find_earliest_times()

        maxtp = -1
        for activity in esa_dict:
            if esa_dict[activity] > maxtp:
                maxtp = esa_dict[activity]
        return maxtp

    def calculate_percent_reduction(self):
        max_tp = self.max_tp()
        num_acts = len(self.find_earliest_times())
        original_size = num_acts * max_tp

        new_size = 0

        for act in self.find_earliest_times():
            reduction = (self.find_earliest_times()[act] - 1)
            reduced_size = max_tp - reduction 
            new_size += reduced_size

        return (new_size / original_size) * 100

    def _traverse_graph(self, root, elevation):
        if(root == None):
            return {}
        else:
            # When we reach a decline node, update our elevation to be deeper
            if(is_decline(root)):
                elevation = decline_elevation(root)

            times_dict =  {root: self._time_to_elevation(elevation)}

            # For all paths that could be followed after this action
            for succ in self._graph.successors(root):

                # Find the times for those actions, recursively
                succ_dict = self._traverse_graph(succ, elevation) 

                # Keep only the fastest path to each of those actions in the dict
                times_dict = merge_dicts(times_dict, succ_dict)

            return times_dict

    def _time_to_elevation(self, elevation):
        if(self._max_elevation == None):
            raise ValueError('Max elevation value not properly set! Cannot determine delta.')

        # Number of years to travel down a certain distance, rounded up
        # can be thought of as the year during which this elevation could be
        # achieved
        
        # special case: no distance
        if(elevation == self._max_elevation):
            return 1

        return (math.ceil( (self._max_elevation - elevation) / self._depth_per_month / 12))

    def _print_succ(self, root, depth):
        if(root != None):
            print('   '*depth, root)
            for succ in self._graph.successors(root):
                print_succ(succ, depth+1)

    def _activity_to_panel(self, activity):
        for panel in self._panels_acts:
            if activity in self._panels_acts[panel]:
                return panel
        return None
