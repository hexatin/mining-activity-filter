import time

debugMode = False

# Prints only if debugmode flag is activated
def debugp(msg):
    global debugMode
    if debugMode:
        print(msg)

# Generate timestamp of current time in form
# YYYY-MM-DD_HH-MM-SS
def gen_timestamp():
    return time.strftime('%Y-%m-%d_%H-%M-%S') 

# Consumes early-start dictionary produced by an activity filter object
# Prints it into the form required
def print_to_file(earlystart_dict, acfil):
    ts = gen_timestamp()

    # EarlyStart Activity
    with open('outfiles/esa-{}.dat'.format(ts), 'w') as outfile:
        outfile.write('param esa :=\n')
        for activity in earlystart_dict:
            outfile.write(f'{activity} {earlystart_dict[activity]}\n')
        outfile.write(';')

    # EarlyStart Panel

    # Convert earlystart activities into their corresponding panels
    esp_dict = {}
    for activity in earlystart_dict:
        if(not is_decline(activity) and not is_levdev(activity)):
            pnl = acfil._activity_to_panel(activity)
            # Make sure no panels are repeated
            if (
                (pnl not in esp_dict)
                or (earlystart_dict[activity] < esp_dict[pnl]) 
            ):
                esp_dict[pnl] = earlystart_dict[activity]

    with open('outfiles/esp-{}.dat'.format(ts), 'w') as outfile:
        outfile.write('param esp :=\n')
        # Put panel strings in a temp list for sorting
        lst = []
        for panel in esp_dict:
            lst.append(f'{panel} {esp_dict[panel]}\n')
        # Print panel strings
        for ss in sorted(lst):
            outfile.write(ss)
        outfile.write(';')

# Determined from activity name
def is_decline(node):
    return node[:7].lower() == 'decline'

# Determined from activity name
def is_levdev(node):
    return node[:6].lower() == 'levdev'

# Retrieve elevation from the name of a decline activity
def decline_elevation(decline_node):
    return int(decline_node.split('_')[-1])

def merge_dicts(a, b):
    debugp(f'DICT A:\n{a}')
    debugp(f'DICT B:\n{b}')
    merged_dict = {}

    # Look through all activities in A
    # All activities that are only in A or are faster than in B go to merged
    for activity in a:
        if (activity not in b):
            merged_dict[activity] = a[activity]
        elif (a[activity] < b[activity]):
            merged_dict[activity] = a[activity]
        else:
            merged_dict[activity] = b[activity]

    # Look through activities in b
    # All activities that are only in B, which slipped through the cracks last
    # time, will be put in merged
    for activity in b:
        if (activity not in a):
            merged_dict[activity] = b[activity]

    return merged_dict
