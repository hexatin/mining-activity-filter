from helpers import *
import activityfilter

import argparse

def main():
    # step 1: parse the input documents for the necessary information
    # step 2: put all activities into some sort of graph
    # step 3: calculate the earliest possible time for all activities based on decline
    # The template for each activity name is: {type of activity}_{method of activity}_{associated decline}_{activity number}_{associated panel}

    #- do not grant access to a level until the time period after a decline has passed the required floor elevation of a level.
    #- Levels are 25 meters in height.

    ############################
    # SETUP GLOBALS/CONSTANTS
    ############################

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'inputfile',
        metavar='<inputfile>',
        type=str,
        help='Path to input .dat file'
    )
    parser.add_argument('--speed', metavar='<speed of excavation in meters per month>', type=int, help='Total distance (in m) that a decline excavates in a month.')
    parser.add_argument('--ratio', metavar='<depth-to-forward ratio>', type=float, help='Ratio (as a decimal) of downward progress to forward progress for a decline.')
    args = parser.parse_args()

    input_dir = args.inputfile

    # Optional variables
    spd = args.speed
    ratio = args.ratio

    ############################
    # BEGIN
    ############################

    # Create activity filter object using input data file and command line options
    acfil = activityfilter.ActivityFilter(input_dir, meters_per_month=spd, depth_to_forward_ratio=ratio)

    # Find early start from activity filter and print to output
    print_to_file(acfil.find_earliest_times(), acfil)

    print('Reduced problem size by ', acfil.calculate_percent_reduction(), '%')

main()
