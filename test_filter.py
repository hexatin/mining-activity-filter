import unittest
import argparse

from helpers import *
import activityfilter

class TestActivityFilterCalculations(unittest.TestCase):

    def setUp(self):
        # Using small test input file
        input_dir = '/home/hexatin/dev/mining-activity-filter/examples/DataFile_Small3.dat'

        self.af = activityfilter.ActivityFilter(input_dir)


    def test_time_to_elevation(self):
        # depth = (1200t)/14
        # t = (14d)/1200

        # h = d/14
        # h = (1200(t-1))/14
        # d = 14h
        # 1200t = 14h

        # using default values of 100 meters/month, 1/14 down/forward
        mpm = 100
        ratio = 1/14
        level_height = 20
        
        # Should reach starting elevation within first time period
        # Verify special case
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation), 1)

        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-20), 1)
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-40), 1)
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-60), 1)
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-80), 1)

        # Test boundary between time periods
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-85), 1)
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-86), 2)

        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-100), 2)
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-120), 2)
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-140), 2)
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-160), 2)

        # Test boundary between time periods
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-171), 2)
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-173), 3)

        # Level should only be marked as complete in the time period after it is all done
        self.assertEqual(self.af._time_to_elevation(self.starting_elevation-180), 3)

if __name__ == '__main__':
    unittest.main()
