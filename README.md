# mining-activity-filter

To run the application:

Run `python3 main.py <input file>` where `input file` is the path to a .dat file containing activities and precedence information.

To see other optional command-line parameters to the script, run `python3 main.py` without any arguments to bring up the help message.
